{{/*
redefine gitlab.psql.host
postgres chart re-override postgres.fullname and the final postgres install
does not matches what gitlab expects
*/}}
{{- define "gitlab.psql.host" -}}
{{- if .Values.global.psql.host -}}
{{ .Values.global.psql.host }}
{{- else -}}
{{ include "gitlab.other.fullname" ( dict "context" . "chartName" "postgresql" ) -}}-postgresql
{{- end -}}
{{- end -}}
