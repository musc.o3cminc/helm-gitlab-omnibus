{{/*
  Generates smtp settings for retrieving the password from gitlab-omnibus
*/}}
{{- define "freedesktop.smtp_password" -}}
{{ coalesce .Values.global.omnibus.smtpPassword .Values.smtp.password }}
{{- end -}}

{{/*
  Generates postgres settings for retrieving the password from gitlab-omnibus
*/}}
{{- define "freedesktop.postgres_password" -}}
{{ coalesce .Values.global.omnibus.postgresPassword .Values.psql.password }}
{{- end -}}

{{/*
  Generates omniauth settings for retrieving the secrets from gitlab-omnibus
*/}}
{{- define "freedesktop.oauthGoogleSecret" -}}
{{ coalesce .Values.global.omnibus.oauthGoogleSecret .Values.oauth.google.secret }}
{{- end -}}

{{- define "freedesktop.oauthGoogleId" -}}
{{ coalesce .Values.global.omnibus.oauthGoogleId .Values.oauth.google.id }}
{{- end -}}

{{- define "freedesktop.oauthGitLabSecret" -}}
{{ coalesce .Values.global.omnibus.oauthGitLabSecret .Values.oauth.gitlab.secret }}
{{- end -}}

{{- define "freedesktop.oauthGitLabId" -}}
{{ coalesce .Values.global.omnibus.oauthGitLabId .Values.oauth.gitlab.id }}
{{- end -}}

{{- define "freedesktop.oauthGitHubSecret" -}}
{{ coalesce .Values.global.omnibus.oauthGitHubSecret .Values.oauth.github.secret }}
{{- end -}}

{{- define "freedesktop.oauthGitHubId" -}}
{{ coalesce .Values.global.omnibus.oauthGitHubId .Values.oauth.github.id }}
{{- end -}}

{{- define "freedesktop.oauthTwitterSecret" -}}
{{ coalesce .Values.global.omnibus.oauthTwitterSecret .Values.oauth.twitter.secret }}
{{- end -}}

{{- define "freedesktop.oauthTwitterId" -}}
{{ coalesce .Values.global.omnibus.oauthTwitterId .Values.oauth.twitter.id }}
{{- end -}}

{{/*
  Generates gcs settings for retrieving the secrets from gitlab-omnibus
*/}}
{{- define "freedesktop.registryGCSBucket" -}}
{{ coalesce .Values.global.omnibus.registryGCSBucket .Values.gcs.registry.bucket }}
{{- end -}}

{{- define "freedesktop.registryGCSKey" -}}
{{ coalesce .Values.global.omnibus.registryGCSKey .Values.gcs.registry.key }}
{{- end -}}

{{- define "freedesktop.artifactGCSKey" -}}
{{ coalesce .Values.global.omnibus.artifactGCSKey .Values.gcs.artifacts.key }}
{{- end -}}

{{- define "freedesktop.backupGCSKey" -}}
{{ coalesce .Values.global.omnibus.backupGCSKey .Values.gcs.backups.key }}
{{- end -}}

{{- define "freedesktop.uploadGCSKey" -}}
{{ coalesce .Values.global.omnibus.uploadGCSKey .Values.gcs.uploads.key }}
{{- end -}}

{{- define "freedesktop.lfsGCSKey" -}}
{{ coalesce .Values.global.omnibus.lfsGCSKey .Values.gcs.lfs.key }}
{{- end -}}

{{- define "freedesktop.packageGCSKey" -}}
{{ coalesce .Values.global.omnibus.packageGCSKey .Values.gcs.packages.key }}
{{- end -}}

{{- define "freedesktop.backupLegacyGCSKey" -}}
{{ coalesce .Values.global.omnibus.backupLegacyGCSKey .Values.gcs.backupLegacy.key }}
{{- end -}}

{{- define "freedesktop.backupLegacyGCSSecret" -}}
{{ coalesce .Values.global.omnibus.backupLegacyGCSSecret .Values.gcs.backupLegacy.secret }}
{{- end -}}
